# ------STEP01-------
## Update timzone
```
ln -sf /usr/share/zoneinfo/Asia/Ho_Chi_Minh /etc/localtime
apt update && apt -y upgrade && apt install lsb-release
```
## PHP install
```
apt -y install curl apt-transport-https ca-certificates
wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > \
/etc/apt/sources.list.d/php.list
apt update && apt -y install php7.4 php7.4-curl php7.4-cli php7.4-mysql php7.4-mbstring php7.4-gd php7.4-xml
```

## Packages dependencies
```
apt -y install locales sngrep build-essential aptitude openssh-server apache2 mariadb-server mariadb-client bison doxygen flex php-pear curl sox libncurses5-dev libssl-dev libmariadb-dev mpg123 libxml2-dev libnewt-dev sqlite3 libsqlite3-dev pkg-config automake libtool-bin autoconf git subversion uuid uuid-dev libiksemel-dev tftpd postfix mailutils nano ntp libspandsp-dev libcurl4-openssl-dev libical-dev libneon27-dev libasound2-dev libogg-dev libvorbis-dev libicu-dev libsrtp*-dev unixodbc unixodbc-dev python-dev xinetd e2fsprogs dbus sudo xmlstarlet lame ffmpeg dirmngr linux-headers* gnupg2
```

## Nodejs install
```
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
apt -y install nodejs
```
## ODBC install
```
cd /usr/src
wget https://downloads.mariadb.com/Connectors/odbc/connector-odbc-2.0.19/\
mariadb-connector-odbc-2.0.19-ga-debian-x86_64.tar.gz
tar -zxvf mariadb-connector-odbc-2.0.19*.tar.gz
cp lib/libmaodbc.so /usr/lib/x86_64-linux-gnu/odbc/
```
```
cat >> /etc/odbcinst.ini << EOF
[MySQL]
Description = ODBC for MariaDB
Driver = libmaodbc.so
FileUsage = 1
EOF
```
```
cat >> /etc/odbc.ini << EOF
[MySQL-asteriskcdrdb]
Description = MariaDB connection to 'asteriskcdrdb' database
driver = MySQL
server = localhost
database = asteriskcdrdb
Port = 3306
Socket = /var/run/mysqld/mysqld.sock
option = 3
  
EOF
```
## MongoDB install (option)
```
wget -qO - https://www.mongodb.org/static/pgp/server-5.0.asc | sudo apt-key add -
echo "deb http://repo.mongodb.org/apt/debian buster/mongodb-org/5.0 main" \
| sudo tee /etc/apt/sources.list.d/mongodb-org-5.0.list
apt update && apt install -y mongodb-org
systemctl enable mongod
```
## Asterisk install
```
cd /usr/src
wget http://downloads.asterisk.org/pub/telephony/asterisk/asterisk-18-current.tar.gz
tar zxvf asterisk-18-current.tar.gz
cd /usr/src/asterisk-18*/
make distclean
cd /usr/src/asterisk-18*/
./contrib/scripts/install_prereq install
./configure --with-jansson-bundled
make menuselect.makeopts
menuselect/menuselect --enable app_macro menuselect.makeopts
```
### Setting Asterisk ownership permissions.
```
adduser asterisk --disabled-password --gecos "Asterisk User"
make && make install && chown -R asterisk. /var/lib/asterisk
```
# -----------STEP02-----------
## FreePBX install
```
cd /usr/src
git clone -b release/16.0 --single-branch https://github.com/freepbx/framework.git freepbx
touch /etc/asterisk/modules.conf
cd /usr/src/freepbx
./start_asterisk start
./install -n
```
## Minimal module install
```
fwconsole ma downloadinstall framework core voicemail sipsettings infoservices \
featurecodeadmin logfiles callrecording cdr dashboard music soundlang recordings conferences pm2
fwconsole chown
fwconsole reload
```
```
cat >> /etc/systemd/system/freepbx.service << EOF
[Unit]
Description=Freepbx
After=mariadb.service
 
[Service]
Type=oneshot
RemainAfterExit=yes
ExecStart=/usr/sbin/fwconsole start -q
ExecStop=/usr/sbin/fwconsole stop -q
 
[Install]
WantedBy=multi-user.target

EOF
```
```
systemctl enable freepbx
```
## Configure Apache
```
cat >> /etc/apache2/conf-available/allowoverride.conf << EOF 
<Directory /var/www/html>
    AllowOverride All
    </Directory>
EOF
```
```
a2enconf allowoverride
sed -i 's/\(APACHE_RUN_USER=\)\(.*\)/\1asterisk/g' /etc/apache2/envvars
sed -i 's/\(APACHE_RUN_GROUP=\)\(.*\)/\1asterisk/g' /etc/apache2/envvars
chown asterisk. /run/lock/apache2
mv /var/www/html/index.html /var/www/html/index.html.disable
a2enmod rewrite
systemctl restart apache2
systemctl enable apache2
```
## Log File Rotation
```
cat >> /etc/logrotate.d/asterisk << EOF
/var/spool/mail/asterisk
/var/log/asterisk/full
/var/log/asterisk/dtmf
/var/log/asterisk/fail2ban
/var/log/asterisk/freepbx.log
/var/log/asterisk/freepbx_security.log 
/var/log/asterisk/freepbx_debug {
        size 50M
        missingok
        rotate 4
        #compress
        notifempty
        sharedscripts
        create 0640 asterisk asterisk
        postrotate
        /usr/sbin/asterisk -rx 'logger reload' > /dev/null 2> /dev/null || true
        endscript
        su root root
}
EOF
```
```
sed -i 's/memory_limit = .*/memory_limit = 256M/g' /etc/php/7.4/apache2/php.ini
sed -i 's/upload_max_filesize = .*/upload_max_filesize = 20M/g' /etc/php/7.4/apache2/php.ini
systemctl restart apache2
systemctl disable apparmor
systemctl stop apparmor
```
```
reboot
```